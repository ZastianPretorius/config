#!/bin/bash

quote=$(fortune -n 120 -s)
i3lock --ringcolor 8218c4 --keyhlcolor 3e0c60  --blur 15 --composite --pass-media-keys --pass-volume-keys -S 1 -k --insidevercolor 8218c4 --ringvercolor 3e0c60 --{time,date,layout,verif,wrong}size=40 --radius 200 --greetertext="$quote" --{time,date,layout,verif,wrong,greeter}-font=ubuntu
