;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #55992299
foreground = #282828
background-alt = #99444444
foreground-alt = #ff0000
primary = #8D2C9E
secondary = #A935BD
alert = #FF0000

[bar/barbase]
width = ${env:width}
offset-x = ${env:offsetx}
monitor = ${env:monitor}
;offset-y = ${env:y}
;width = 100%
height = 27
fixed-center = false
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2
;font-0 = misc fixed:pixelsize=10;1
;font-1 = unifont:fontformat=truetype:size=10:antialias=false;0
;font-2 = siji:pixelsize=10;1
;font-3 = Iosevka:pixelsize=13
;font-4 = FontAwesome:pixelsize=20;1
font-0 = "UbuntuMono Nerd Font:size=10;2"
font-1 = "UbuntuMono Nerd Font:size=16;3"
font-2 = "Font Awesome 5 Free:style=Regular:pixelsize=13;1"
font-3 = "Font Awesome 5 Free:style=Solid:pixelsize=13;1"
font-4 = "Font Awesome 5 Brands:pixelsize=13;1"
font-5 = siji:pixelsize=10;1

label-font = 
modules-left = ewmh
modules-center = title-xmonad
modules-right = filesystem pkgupdates-available pulseaudio memory cpu battery date kdeconnect powermenu
tray-position = right
tray-padding = 2
cursor-click = pointer
cursor-scroll = ns-resize

[module/xmonad]
type = custom/script
exec = tail -F /tmp/.xmonad-workspace-log
;exec-if = [ -p /tmp/.xmonad-workspace-log ]
tail = true

[module/title-xmonad]
type = custom/script
exec = tail -F /tmp/.xmonad-title-log
;exec-if = [ -p /tmp/.xmonad-title-log ]
tail = true


[module/ewmh]
type = internal/xworkspaces


pin-workspaces = false
enable-click = true
enable-scroll = false

;["", "", "", "", "", "", "", "", ""]
icon-0 = 1:MAIN;1:
icon-1 = 2:DEV;2:
icon-2 = 3:WWW;3:
icon-3 = 4:TERM;4:
icon-4 = 5:TEXT;5:
icon-5 = 6:FOLDER;6:
icon-6 = 7:SCHOOL;7:
icon-7 = 8:GAMES;8:
icon-8 = 9:MUSIC;9:


format = <label-state>
;label-monitor = %name%
;label-monitor-underline = ${colors.cyan}

label-active = %icon%
label-active-foreground = ${colors.green}
label-active-underline =  #ff0000
label-active-padding = 1

label-occupied = %icon%
label-occupied-foreground = ${colors.blue}
label-occupied-underline = #00ff00
label-occupied-padding = 1

label-urgent = %icon%
label-urgent-foreground = ${colors.red}
label-urgent-underline = #0000ff
label-urgent-padding = 1

label-empty = %icon%
label-empty-foreground = ${colors.fg}
label-empty-underline = ${colors.cyan}
label-empty-padding = 1


[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /home

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

; Separator in between workspaces
; label-separator = |

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

; Separator in between workspaces
; label-separator = |


[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #8d2c9e
label = CPU %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #8d2c9e
label = RAM %percentage_used%%

[module/wlan]
type = internal/network
interface = wlp2s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #8d2c9e
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = enp3s0
interval = 3.0

format-connected-underline = #8d2c9e
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
date =    %%{F#99}%m/%d/%Y%%{F-}  %%{F#99}%I:%M %p%{F-}
date-alt = %%{F#99}%A, %B %d, %Y  %%{F#99}%I:%M %p%{F#99}%%{F#99}%S%%{F-}
;interval = 5
;date =
;date-alt = " %Y-%m-%d"
;time = %H:%M
;time-alt = %H:%M:%S
;format-prefix = 
;format-prefix-foreground = ${colors.foreground}
;format-underline = #0a6cf5
;label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 6
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98
label-charging =  %percentage%%
label-discharging =  %percentage%%

format-charging = <animation-charging> <label-charging>
format-charging-underline = #8d2c9e

format-discharging =<animation-discharging> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750


[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = #f50a4d
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

[module/powermenu]
type = custom/menu


expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2
menu-0-2 = logout
menu-0-2-exec = menu-open-3

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = reboot

menu-2-0 = power off
menu-2-0-exec = poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

menu-3-0 = logout
menu-3-0-exec = pkill xmonad
menu-3-1 = cancel
menu-3-1-exec = menu-open-0

[module/kdeconnect]  
type = custom/script  
exec = "/home/mrfluffy/.config/script/polybar-kdeconnect.sh -d"  
tail = true

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[module/pkgupdates-available]
type = custom/script

; Available tokens:
;   %counter%
; Command to be executed (using "/bin/sh -c [command]")
exec = /home/mrfluffy/.config/script/updateCount.sh 

interval = 3600


;[global/wm]
;margin-top = 5
;margin-bottom = 5

; vim:ft=dosini
